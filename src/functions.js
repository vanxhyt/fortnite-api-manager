const axios = require('axios')

const accountServices = `https://account-public-service-prod.ol.epicgames.com/account/`
const iosAUTH = 'MzQ0NmNkNzI2OTRjNGE0NDg1ZDgxYjc3YWRiYjIxNDE6OTIwOWQ0YTVlMjVhNDU3ZmI5YjA3NDg5ZDMxM2I0MWE='

async function generateToken(accountId, deviceId, secret) {
    var data = 'grant_type=device_auth&account_id=' + accountId + '&device_id=' + deviceId + '&secret=' + secret;
    var config = {
        method: 'post',
        url: `${accountServices}api/oauth/token`,
        headers: {
            'Content-Type': ' application/x-www-form-urlencoded',
            'authorization': `basic ${iosAUTH}`
        },
        data: data
    }
    try {
        let response = (await axios(config)).data
        return response['access_token']
    } catch (e) {
        return false
    }
}

module.exports = {
  generateToken
}