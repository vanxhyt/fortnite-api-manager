 /**
 * fortnite-manager
 * Module to make API calls to fortnite
 *
 * @author Vanxh
 */
 
 /**
 * index.js
 * Export the Client
 */
 const Client = require("./src/index");
 module.exports = Client;